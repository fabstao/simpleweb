# Simple Demo Web
## Introduction
This is a simple demo web page that will show the hostname and remote client IP address
## Use cases
Demos around cloud, network loadbalancing, containers and similar environments
## Requirements
- Go 1.16
- Docker or Podman
- GNU Make
- bash
## Usage
Use Makefile with GNU make:
For building everything:
```
$ make all
```
Cleaning the environment:
```
$ make clean
```
Building container image:
```
$ make container
```
## Author
Fabs
