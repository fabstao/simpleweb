#****************************
#* Demo Dockerfile
#****************************

FROM alpine:latest

ENV WDIR=/opt/simpleweb
RUN apk update && apk upgrade
RUN apk add bash gcompat
RUN adduser -h $WDIR -D -s /bin/bash simpleweb
USER simpleweb
WORKDIR $WDIR 
RUN mkdir -p $WDIR/views
COPY simpleweb $WDIR
ADD views $WDIR/views

EXPOSE 3780

ENTRYPOINT ["./simpleweb"]
