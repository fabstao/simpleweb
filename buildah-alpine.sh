#!/bin/bash
echo "Starting"
BUILDAH=/usr/bin/buildah
PODMAN=/usr/bin/podman

BASE=$(${BUILDAH} from docker.io/alpine )
echo ${BASE}
WDIR=/opt/simpleweb
${BUILDAH} run ${BASE} -- apk update
${BUILDAH} run ${BASE} -- apk upgrade
${BUILDAH} run ${BASE} -- apk add bash
${BUILDAH} run ${BASE} -- apk add gcompat
${BUILDAH} run ${BASE} -- ls -lrth /
${BUILDAH} run ${BASE} -- mkdir -p ${WDIR}
${BUILDAH} run ${BASE} -- mkdir -p ${WDIR}/views
${BUILDAH} run ${BASE} -- ls -lrth /opt
${BUILDAH} run ${BASE} -- ls -lrth ${WDIR}
${BUILDAH} copy ${BASE} simpleweb ${WDIR}
${BUILDAH} copy ${BASE} views ${WDIR}/views/
${BUILDAH} config --port 3180/tcp ${BASE}
${BUILDAH} config --workingdir ${WDIR} ${BASE} 
${BUILDAH} config --entrypoint "./simpleweb" ${BASE} 
${BUILDAH} commit ${BASE} docker.io/fabstao/democonweb
