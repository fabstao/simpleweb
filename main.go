package main

import (
    "log"
	"os"
	"fmt"
    "github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/html"
)

func main() {
	engine := html.New("./views", ".html")
	app := fiber.New(fiber.Config{
		Views: engine,
	})
	hostname := gethostn()
	Port := "3780"
    app.Get("/", func (c *fiber.Ctx) error {
		//hhost := string(c.Request().Header.Header())
		hhost := c.Context().RemoteIP()
		log.Println(hhost)
		return c.Render("index", fiber.Map{
			"Title": "Demo Web for load balancing, Kubernetes and cloud environments",
			"Hostname": hostname,
			"Client": hhost,
		}, "layouts/main")
    })

    log.Fatal(app.Listen(":" + Port))
}

func gethostn() string {
	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return hostname
}
