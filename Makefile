CONTAINER=docker.io/fabstao/demowebcon

all: build container

build:
	go build -o simpleweb *.go

clean:
	rm -f simpleweb

container:
	./buildimage.sh ${CONTAINER}
