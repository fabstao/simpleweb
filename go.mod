module github.com/fabstao/simpleweb

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.26.0 // indirect
	github.com/gofiber/template v1.6.22 // indirect
	github.com/klauspost/compress v1.14.2 // indirect
	github.com/valyala/fasthttp v1.33.0 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
