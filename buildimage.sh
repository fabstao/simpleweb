#!/bin/bash
#
# This is intended to be called from the main Makefile

CONTAINER=$1
DOCKER=$(which docker)
PODMAN=$(which podman)
if [ -f "${DOCKER}" ]; then
    docker build -t ${CONTAINER} .
    exit 0
fi
if [ -f "${PODMAN}" ]; then
    buildah bud -f Dockerfile ${CONTAINER}
    exit 0
else
	exit 1
fi

